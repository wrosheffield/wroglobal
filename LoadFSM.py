#!/usr/bin/env python
# WRO load n-bests
#Xingyi Song
#x.song@sheffield.ac.uk
from __future__ import division
import sys
import getopt
import re
import math
import random
from simpBLEU_recall_overlength import Obtain_bleu 
import gzip
small=1e-9
tiny=1e-15


def readFile(fileName):
    errorCode=0
    fileLines=[]
    try:
        fFile = open(fileName,'r')
        fileLines = fFile.readlines()
        fFile.close()
    except:
        errorCode = 1
        print 'Can not read feature file ', fileName
    return fileLines, errorCode

def readgzipFile(fileName):
    errorCode=0
    fileLines=[]
    try:
        fFile = gzip.open(fileName,'r')
        fileLines = fFile.readlines()
        fFile.close()
    except:
        errorCode = 1
        print 'Can not read feature file ', fileName
    return fileLines, errorCode


class LoadMERT:
    def __init__(self):
        self.wholeList=[]

    def calculateBLEU(self,scoreStat,n=4,smoothing=float(1.0)):
        #print self.scoreList[i][k]
        logbleu=0.0
        for norder in range(n):
            logbleu += math.log(scoreStat[2*norder]+tiny+smoothing) - math.log(scoreStat[2*norder+1]+small+smoothing)
        logbleu /= n
        ratio = (scoreStat[1]+tiny)/(scoreStat[8]+small)
        if ratio < 1:
            logbleu += (1-1/ratio)
        return math.exp(logbleu)#, math.exp(logbleu)

    def loadScores(self,currentScores):
        scoreList=[]
        startRead=0
        for line in currentScores:
            line=line.strip()
            beginMatch=re.match('SCORES_TXT_BEGIN_0', line)
            endMatch=re.match('SCORES_TXT_END_0',line)
            if endMatch:
                startRead=0
                sentId=numCand=numScoreStat=0
                scorerType=None

            if startRead == 1:
                tmpList=[]
                lineTok=line.split()
                if len(lineTok) == numScoreStat:
                    for tok in lineTok:
                        tmpList.append(int(tok))
                    bleuScores= self.calculateBLEU(tmpList)
                    scoreList[sentId].append(bleuScores)
                #   print scoreList[sentId][-1]
                else:
                    print 'Number of fields do not match'
                    sys.exit()

            if beginMatch:
                #print line
                startRead=1
                scoreList.append([])
                lineTok=line.split()
                sentId=int(lineTok[1])
                numCand=int(lineTok[2])
                scorerType=lineTok[4]
                numScoreStat=int(lineTok[3])
        return scoreList

    def loadFeatures(self,currentFeatures):
        featureList=[]
        startRead=0
        for line in currentFeatures:
            line=line.strip()
            beginMatch=re.match('FEATURES_TXT_BEGIN_0', line)
            endMatch=re.match('FEATURES_TXT_END_0', line)
            if endMatch:
                startRead=0
                sentId=numCand=numFeatureStat=0

            if startRead == 1:
                tmpList=[]
                lineTok=line.split()
                if len(lineTok) == numFeatureStat:
                    for tok in lineTok:
                        tmpList.append(float(tok))
                    featureList[sentId].append(tmpList)
                else:
                    print 'Number of fields do not match'
                    sys.exit()

            if beginMatch:
                startRead=1
                featureList.append([])
                lineTok=line.split()
                sentId=int(lineTok[1])
                numCand=int(lineTok[2])
                numFeatureStat=int(lineTok[3])
        return featureList

    def loadNbests(self,currentNbest):
        featureList=[]
        sentList=[]

        for line in currentNbest:
            tok=line.split(' ||| ')
            #print tok
            currentSentId=int(tok[0].strip())
            currentSent=tok[1]
            subModelScores=tok[2]
            currentFeatureList=self.readSubModelScore(subModelScores)
            if len(featureList) <= currentSentId:
                featureList.append([])
                sentList.append([])
            featureList[currentSentId].append(currentFeatureList)
            sentList[currentSentId].append(currentSent)
        return sentList, featureList

    def readSubModelScore(self,subModelScores):
        tmpFeatureList=[]
        noneScoreTokens=['WordPenalty0=','LM0=','Distortion0=','LexicalReordering0=','PhrasePenalty0=','TranslationModel0=']
        subModelToks=subModelScores.split()
        for item in subModelToks:
            if item not in noneScoreTokens:
                tmpFeatureList.append(float(item))
        return tmpFeatureList


    def combineFSlists(self, currentFeatureList,currentScoreList):
        if self.wholeList:
            for i in range(len(currentScoreList)):
                for j in range(len(currentScoreList[i])):
                    if [currentFeatureList[i][j],currentScoreList[i][j][0],currentScoreList[i][j][1]] not in self.wholeList[i]:
                        self.wholeList[i].append([currentFeatureList[i][j],currentScoreList[i][j][0],currentScoreList[i][j][1]])
        else:
            for i in range(len(currentScoreList)):
                self.wholeList.append([])
                for j in range(len(currentScoreList[i])):
                    if [currentFeatureList[i][j],currentScoreList[i][j][0],currentScoreList[i][j][1]] not in self.wholeList[i]:
                        self.wholeList[i].append([currentFeatureList[i][j],currentScoreList[i][j][0],currentScoreList[i][j][1]])



class LoadFS(LoadMERT):
    def importFS(self, featureFiles, scoreFiles):
        if len(featureFiles) != len(scoreFiles):
            print 'Number of feature files and score files does not match'
            sys.exit()

        print 'start read files'

        for i in range(len(featureFiles)):
            print 'reading '+str(i+1)+'th file'
            currentScoreList,currentFeatureList = self.readFSfiles(featureFiles[i], scoreFiles[i])
            if len(currentScoreList) != len(currentFeatureList):
                print 'Number of features do not match number of scores at file ', featureFiles[i], scoreFiles[i]
                sys.exit()
            self.combineFSlists(currentFeatureList,currentScoreList)
        return self.wholeList

    def readFSfiles(self, featureFile, scoreFile):
        # ensure the files at the same run
        fnameTok=featureFile.split('.')
        snameTok=scoreFile.split('.')
        if fnameTok[0] != snameTok[0]:
            print 'Feature file and Score file are not in the same run, if you have multiple files to input please place them as same order.'
            sys.exit()
        try:
            fFeature=open(featureFile,'r')
            currentFeatures=fFeature.readlines()
            fFeature.close()
        except:
            print 'Can not read feature file ', featureFile

        try:
            fScore=open(scoreFile,'r')
            currentScores=fScore.readlines()
            fScore.close()
        except:
            print 'Can not read score file ', scoreFile

        currentScoreList=self.loadScores(currentScores)
        currentFeatureList=self.loadFeatures(currentFeatures)
        return currentScoreList, currentFeatureList


class LoadNbests(LoadMERT):
    def __init__(self, refList):
        self.refList=refList
        self.wholeList=[]

    def importFS(self, nbestFileNames):
        for i in range(len(nbestFileNames)):
            print 'reading file ' + nbestFileNames[i]
            currentScoreList,currentFeatureList = self.readNbest(nbestFileNames[i])
            if len(currentScoreList) != len(currentFeatureList):
                print 'Number of features do not match number of scores at file ', featureFiles[i], scoreFiles[i]
                sys.exit()
            self.combineFSlists(currentFeatureList,currentScoreList)
        return self.wholeList
        
    def readNbest(self, nbestFileName):
        currentNbest, errorCode = readgzipFile(nbestFileName)
        if errorCode == 1:
            print 'Can not read nbest file ', nbestFileName
            sys.exit()
        currentSentList, currentFeatureList=self.loadNbests(currentNbest)
        currentScoreList=self.calculateMetricScore(currentSentList)
        return currentScoreList,currentFeatureList
        
    def calculateMetricScore(self,currentSentList):
        scoreList=[]
        for i in range(len(currentSentList)):
            if len(scoreList) <= i:
                scoreList.append([])
            for sent in currentSentList[i]:
                MetricScore=self.extenalmetrics(sent,self.refList[i])
                scoreList[i].append(MetricScore)
        return scoreList

    def extenalmetrics(self,sent,ref):
        if ref[-1] == '\n':
            ref=ref[:-1]
        score = Obtain_bleu().bleu_sentence(sent,ref)
        return score, score






