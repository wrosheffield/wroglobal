#!/usr/bin/env python
#WRO-global version 1
#Xingyi Song
#x.song@sheffield.ac.uk
from __future__ import division
import sys
from LoadFSM import LoadNbests
from ProSample_new import ProSample_pro
import cPickle as cpickle
import getopt
import re
import math
import random
import os
import ConfigParser
from mpi4py import MPI

def funcSaveData(data2save,fileName):
    fileSave = open(fileName,'wb')
    cpickle.dump(data2save,fileSave)
    fileSave.close()

def funcLoadData(fileName):
    fileLoad=open(fileName,'r')
    data2Load=cpickle.load(fileLoad)
    fileLoad.close()
    return data2Load

def createScatTestList(itemList, numProcess):
    scatDesList=[]
    workerDataSize=int(math.floor(len(itemList)/numProcess))
    #print workerDataSize
    pidx=0
    for i in range(1,numProcess):
        cidx=i*workerDataSize
        scatDesList.append(itemList[pidx:cidx])
        pidx=cidx
    scatDesList.append(itemList[cidx:])
    #scatDesList=np.array(scatDesList)
    return scatDesList


def createScatItemList(itemList,wholeList, numProcess):
    scatDesList=[]
    scatWholeList=[]
    workerDataSize=int(math.floor(len(itemList)/numProcess))
    #print workerDataSize
    pidx=0
    for i in range(1,numProcess):
        cidx=i*workerDataSize
        scatDesList.append(itemList[pidx:cidx])
        scatWholeList.append(wholeList[pidx:cidx])
        pidx=cidx

    scatDesList.append(itemList[cidx:])
    scatWholeList.append(wholeList[cidx:])
    print len(scatDesList[0]), len(scatDesList[1])
    print len(scatDesList[0]), len(scatDesList[1])
    
    #scatDesList=np.array(scatDesList)
    return scatDesList, scatWholeList

def readFiles(inputFile):
    fin=open(inputFile,'r')
    sentsList=fin.readlines()
    fin.close()
    return sentsList

def extraWeights(learningMethod,modelFile,outputFile):
    if learningMethod=='svm':
        os.system('tail -1 '+modelFile+' > '+outputFile)    
    if learningMethod=='mart':
        fmodelin=open(modelFile,'r')
        modelIn=fmodelin.readlines()
        fmodelin.close()
        outLine='0 '+modelIn[-1]
        fout=open(outputFile,'w')
        fout.write(outLine)
        fout.close()



if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    name = MPI.Get_processor_name()
    comm.Barrier()
    scatSentList=[]
    scatWholeList=[]
    wholeList=[]
    devListE=[]
    devListF=[]
    alignList=[]
    jdList=[]
    qdList=[]
    scatIdList=[]
    simpleFeatureList=[]
    useage = 'python wpro_lite.py <foreign_tuning_file> <english_tuning_file> --nBestFiles <list,of,nbestFiles> -o <wroOutputFile>'
    
    if len(sys.argv) < 2:
        print useage
        

    learning_method='pro' 
    trainingSteps=0
    saveData=0
    loadData=0
    fDevFile=sys.argv[1]
    eDevFile=sys.argv[2]

    
    opts, args = getopt.getopt(sys.argv[3:], 't:o:s:',['tstfile=','vocofile=','indexfile=','tfidfw=','learner=','skipSample','saveData','loadData=','nBestFiles='])
    tfidfw=1
    for opt, arg in opts:
        if opt == '-t' or opt == '--tstfile':
            testFile = arg
        elif opt == '-o':
            wprotmpFile = arg
        elif opt == '--tfidfw':
            tfidfw=float(arg)
        elif opt == '--learner':
            learning_method=arg
        elif opt == '--skipSample':
            trainingSteps=1
        elif opt == '--saveData':
            os.system('mkdir -p '+decoderOutDir+'savedData')
            wholeListSave=decoderOutDir+'savedData/wholeList.data'
            saveData=1
        elif opt == '--loadData':
            wholeListLoad=arg
            loadData=1
        elif opt == '--nBestFiles':
            nBestFiles=arg.split(',')


    if trainingSteps==0:
        trainingSteps+=1
        if rank == 0:
            print 'I\'m rank '+str(rank)+' at '+name+', I\'m begin to collecting data.'
            devListE = readFiles(eDevFile)
            devListF = readFiles(fDevFile)

            if loadData==0:
                wholeList = LoadNbests(devListE).importFS(nBestFiles)
            elif loadData==1:
                print 'Loading Data'
                wholeList = funcLoadData(wholeListLoad)
            
            if saveData == 1:
                print 'saving data'
                funcSaveData(wholeList,wholeListSave)                
            print 'Data loading finished'
            print len(wholeList)
            devIdList=range(len(wholeList))
            scatIdList,scatWholeList=createScatItemList(devIdList, wholeList, size)
            print len(scatIdList)
            
        devListE=comm.bcast(devListE,root=0)
        devListF=comm.bcast(devListF,root=0)
        spIdList=comm.scatter(scatIdList,root=0)
        scatWholeList=comm.scatter(scatWholeList,root=0)
        
        comm.Barrier()
        if learning_method=='pro':
            pSample=ProSample_pro(scatWholeList,devListF,devListE,wprotmpFile)
        else:
			print 'not supported other learning method yet'
			sys.exit()
        spSampleList=[]
        for i in range(len(spIdList)):
            spId = spIdList[i]
            currentSampleList=pSample.selection(i,spId)
            spSampleList += currentSampleList
        print 'finish sample'
        comm.Barrier()
        print 'Return sample to 0'
        gatherSampleLists=comm.gather(spSampleList,root=0)
        comm.Barrier()
        
        if rank ==0:
            fullSampleList=[]
            for gsampleList in gatherSampleLists:
                fullSampleList+=gsampleList
            print len(fullSampleList)
            pSample.sampleOut(fullSampleList, wprotmpFile)
