#!/usr/bin/env python
#WRO sample
#Xingyi Song
#x.song@sheffield.ac.uk
from __future__ import division
import sys
import getopt
import re
import math
import random


class ProSample:
    def __init__(self,scatWholeList,devListF,devListE,outputFile):
        self.minBLEUdiff = 0.05
        self.minModelDiff = 0.1
        self.wholeList=scatWholeList
        self.outputFile=outputFile
        self.devListF=devListF
        self.devListE=devListE
        self.numDev=len(devListF)
    
    def genFeature(self, devSentIndex, sampleWeight,m_cand1Id, m_cand2Id, m_score_diff, sortedSingleList):
        try:
            if len(sortedSingleList[m_cand1Id][0]) != len(sortedSingleList[m_cand2Id][0]):
                print 'feature field missing'
                sys.exit()
            posiSample = '1 $$$WEIGHT '+str(sampleWeight)
            negaSample = '0 $$$WEIGHT '+str(sampleWeight)
            t=0
            for i in range(len(sortedSingleList[m_cand1Id][0])):
                iFeatureDiffabs=abs(sortedSingleList[m_cand1Id][0][i] - sortedSingleList[m_cand2Id][0][i])
                if iFeatureDiffabs > 0.00001:
                    iFeatureDiff=sortedSingleList[m_cand1Id][0][i] - sortedSingleList[m_cand2Id][0][i]
                    if iFeatureDiff != 0:
                        posiSample += ' F'+str(i)+' '+str(iFeatureDiff)
                        negaSample += ' F'+str(i)+' '+str(-iFeatureDiff)
                        t+=1
            return posiSample, negaSample, t
        except:
            print m_cand1Id, sortedSingleList[m_cand1Id], m_cand2Id, sortedSingleList[m_cand2Id]
            return 0, 0, 0


class ProSample_pro(ProSample): 
    def selection(self,i,spId):
        currentSampleList=self.sample(i,spId,1)
        return currentSampleList

    def sample(self,wholeListIndex, devSentIndex,sampleWeight,topMaxSample=5000,localSample=50):
        maxSample=topMaxSample
        numNbest=len(self.wholeList[wholeListIndex])
        sortedSingleList=sorted(self.wholeList[wholeListIndex], key=lambda x:x[1],reverse = 1)
        topBLEU=sortedSingleList[0][1]
        botBLEU=sortedSingleList[-1][1]
        BLEUtb=topBLEU-botBLEU
        tbp=math.exp(topBLEU-0.4)
        if tbp > 1:
            tbp = 1

        topSamples=math.floor(localSample*1)
        otherSamples=localSample-topSamples

        sampleCandidateList=[]
        sampleList=[]
        for i in range(2):
            tmpList=[]
            if i == 0:
                topbest=0.1
                #topbest=1
                top50=1
                numlocal=int(topSamples)
            else:
                topbest=1
                top50=1
                numlocal=int(otherSamples)
            #print numlocal    
            for j in range(maxSample):
                cand1Id=random.randint(0,math.floor(((numNbest-1)*topbest)))
                cand2Id=random.randint(0,math.floor(((numNbest-1)*top50)))
                bleuCand1=sortedSingleList[cand1Id][1]
                bleuCand2=sortedSingleList[cand2Id][1]
                diff=bleuCand1-bleuCand2
                #if (abs(diff) > self.minBLEUdiff) and ([cand1Id,cand2Id,diff] not in tmpList):
                if abs(diff) > self.minBLEUdiff:
                    tmpList.append([cand1Id,cand2Id,diff])
            tmpList=sorted(tmpList, key=lambda x:abs(x[2]),reverse = 1)
            sampleCandidateList+=tmpList[:numlocal]
        x=0
        for candidate in sampleCandidateList:
            diff = candidate[2]
            cand1Id = candidate[0]
            cand2Id = candidate[1]
            if diff > 0:
                m_cand1Id = cand1Id
                m_cand2Id = cand2Id
                m_score_diff = diff
            else:
                m_cand1Id = cand2Id
                m_cand2Id = cand1Id
                m_score_diff = -diff
            currentPosiSample, currentNegaSample, t = self.genFeature(devSentIndex,int(math.ceil(((1*tbp+0*m_score_diff)/1)*10)),m_cand1Id, m_cand2Id, m_score_diff,sortedSingleList)
            #currentPosiSample, currentNegaSample, t = self.genFeature(devSentIndex,1,m_cand1Id, m_cand2Id, m_score_diff,sortedSingleList)
            if t > 1:
                sampleList.append(currentPosiSample+'\n')
                sampleList.append(currentNegaSample+'\n')
            x+=1
        return sampleList
    
    def sampleOut(self,sampleList,outputFile):
        fo=open(outputFile,'w')
        for line in sampleList:
            fo.write(line)
        fo.close()

